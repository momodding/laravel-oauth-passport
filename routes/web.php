<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/auth_redirection', function () {
    $query = http_build_query([
        'client_id' => 3, // Replace with Client ID
        'redirect_uri' => 'http://localhost:8080/auth/callback-demo',
        'response_type' => 'code',
        'scope' => ''
    ]);

    return redirect('http://localhost:8080/oauth/authorize?'.$query);
});

Route::get('/callback', function (Request $request) {
    $response = (new GuzzleHttp\Client)->post('http://localhost:8080/oauth/token', [
        'form_params' => [
            'grant_type' => 'authorization_code',
            'client_id' => 3, // Replace with Client ID
            'client_secret' => 'd2r5wqTiaAk8MilcZmSVHIFttrbKfze1rXKGr79B', // Replace with client secret
            'redirect_uri' => 'http://localhost:8080/auth/callback-demo',
            'code' => $request->code,
        ]
    ]);

    session()->put('token', json_decode((string) $response->getBody(), true));

    return redirect('/todos');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

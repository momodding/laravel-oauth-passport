FROM php:7.4-fpm

ARG user
ARG uid

COPY composer.lock composer.json /var/www/
COPY package.json /var/www/
COPY .env .env.example /var/www/

# COPY .env .env.example /var/www/

# Set working directory
WORKDIR /var/www

# Install dependencies
RUN apt-get update && apt-get install -y \
    build-essential \
    libpng-dev \
    libonig-dev \
    libjpeg62-turbo-dev \
    libfreetype6-dev \
    locales \
    zip \
    jpegoptim optipng pngquant gifsicle \
    vim \
    unzip \
    git \
    curl
    # && curl -sL https://deb.nodesource.com/setup_12.x | bash - \
    # && apt-get install -y nodejs

# Clear cache
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# Install extensions
RUN docker-php-ext-install mysqli pdo pdo_mysql mbstring exif pcntl bcmath gd
# RUN docker-php-ext-configure gd --with-gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ --with-png-dir=/usr/include/
RUN pecl install xdebug && docker-php-ext-enable xdebug

# Install composer
# RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

COPY laravel-oauth/php/php-memory-limits.ini /usr/local/etc/php/conf.d/php-memory-limits.ini
COPY laravel-oauth/php/xdebug.ini /usr/local/etc/php/conf.d/xdebug.ini

# RUN npm install

# Add user for laravel application
RUN useradd -G www-data,root -u $uid -d /home/$user $user

# Add user for npm
# RUN chown -R $user:$(id -gn $user) /home/$user/.config

# Copy existing application directory contents
COPY . /var/www

# Copy existing application directory permissions
COPY --chown=$user:$user . /var/www

# Change current user to www
USER $user

# Expose port 9000 and start php-fpm server
EXPOSE 9000
CMD ["php-fpm"]
